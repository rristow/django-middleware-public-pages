from django.apps import AppConfig


class MiddlewarePublicPagesConfig(AppConfig):
    name = 'middleware_public_pages'
