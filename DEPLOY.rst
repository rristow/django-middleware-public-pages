
sources:
========
TEST:  https://test.pypi.org/simple/django-middleware-public-pages/

To deploy:
==========
python3 setup.py sdist bdist_wheel

To upload to test.pypi:
=======================
twine upload --repository-url https://test.pypi.org/legacy/ dist/* 

To test:
========
pip install django-middleware-public-pages --extra-index-url  https://test.pypi.org/simple/
python
>>> from middleware_public_pages import middleware_public_pages

Documentation:
==============
https://docs.djangoproject.com/en/2.1/intro/reusable-apps/
https://packaging.python.org/tutorials/packaging-projects/

